package pl.wroc.pwr.ligretto.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CardSetTest {
	
	private Player createTestPlayer() {
		return new Player("test");
	}
	
	@Test
	public void shouldShuffleCards() {
		
		//given
		Player testPlayer = createTestPlayer();
		testPlayer.getCards();
		testPlayer.shuffleCards();
		int numberOfShuffled = 40;
		boolean shuffled = true;
		
		//when
		for (int i = 1; i < 39; i++)
		{
			
			if ( testPlayer.getCard(i).getValue() == testPlayer.getCard(i + 1).getValue() + 1 )
			{
				if ( testPlayer.getCard(i).getColor() == testPlayer.getCard(i + 1).getColor())
				{
					numberOfShuffled--;
				}
			}
			
		}
		
		if ( numberOfShuffled < 19)
		{
			shuffled = false;
		}
		
		//then
		assertEquals("Cards not shuffled",true,shuffled);
	}

}
