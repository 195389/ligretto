package pl.wroc.pwr.ligretto.model;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Hand {

	public Queue<Card> hand;
	
	public Hand()
	{
		hand = new ArrayBlockingQueue<Card>(27);
	}
}
