package pl.wroc.pwr.ligretto.model;

import java.awt.Color;
import java.util.Stack;

public class Table {

	//static public Stack<Card>[] table;
	static public Stack[] table = new Stack[16];
	static private Card zero_card = new Card(0,Color.GRAY);
	
	
	public Table()
	{
		for (int i = 0; i<16; i++)
		{
			table[i] = new Stack<Card>();
			table[i].push(zero_card);
		}
		
	}
	
	public Card ceckTable(int i) {
		return (Card) table[i].peek();
	}
	
	public void changeTable(Card card, int i) {
		table[i].addElement(card);
	}
	
	
}
