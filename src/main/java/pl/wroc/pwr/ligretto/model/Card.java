package pl.wroc.pwr.ligretto.model;

import java.awt.Color;

public class Card {

	private int value;
	//private int color;
	private Color color;

	
	public Card() {
	}

	public int getValue() {
		return value;
	}

//	public Color getColor() {
//		Color colorStr = null;
//		int colorNum = this.color;
//		
//		if (colorNum == 1) {
//			colorStr = Color.BLUE;
//		}
//		if (colorNum == 2) {
//			colorStr = Color.YELLOW;
//		}
//		if (colorNum == 3) {
//			colorStr = Color.RED;
//		}
//		if (colorNum == 4) {
//			colorStr = Color.GREEN;
//		}
//
//		return colorStr;
//	}

	public Color getColor() {
		return this.color;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Card(int value, Color color)

	{
		this.color = color;
		this.value = value;
	}

	public void showCard() {
		//int value = this.value;
		//int colorNum = this.color;
		//String colorStr = null;

		//if (colorNum == 1) {
		//	colorStr = "Niebieski";
		//}
		//if (colorNum == 2) {
		//	colorStr = "Żółty";
		//}
		//if (colorNum == 3) {
		//	colorStr = "Czerwony";
		//}
		//if (colorNum == 4) {
		//	colorStr = "Zielony";
		//}

		//System.out.println("	# : " + value + " " + colorStr);
	}

}
