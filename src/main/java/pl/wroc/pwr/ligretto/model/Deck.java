package pl.wroc.pwr.ligretto.model;

import java.util.Stack;

public class Deck {

	public Stack<Card> deck;
	
	public Deck()
	{
		deck = new Stack<Card>();
		deck.setSize(10);
	}
	
}
