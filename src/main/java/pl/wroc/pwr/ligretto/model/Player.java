package pl.wroc.pwr.ligretto.model;

import java.awt.Color;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Player {

	private String name;
	private int points;
	private boolean move;
	//private Card[] cards;
	private ArrayList<Card> cards;
	private Hand player_hand;
	private Slot player_slot;
	private Deck player_deck;
	
	public Player(String name)
	{
		this.name = name;
		this.move = false;
		this.points = 0;
		//this.cards = new Card[40];
		this.player_slot = new Slot();
		this.player_deck = new Deck();
		this.player_hand = new Hand();
		this.cards = new ArrayList<Card>();
	}
	
	public String  getName()
	{
		return this.name;
	}
	
	public Card getCard(int i) {
		
		return this.cards.get(i);
	}
	
	public Card checkTopOfDeck()
	{
		return player_deck.deck.peek();
	}
	
	public Card removeTopOfDeck()
	{
		return player_deck.deck.pop();
	}
	
	public Card checkSlot(int i)
	{
		return player_slot.slot[i];
	}
	
	public Card removeSlot(int i)
	{
		Card fromSlot = null;
		fromSlot = player_slot.slot[i];
		player_slot.slot[i] = player_deck.deck.pop();
		return fromSlot;
	}
	
	public Card checkHand()
	{
		//Card first;
		//Card second;
		
		//first = player_hand.hand.remove();
		//second = player_hand.hand.remove();
		
		//player_hand.hand.add(first);
		//player_hand.hand.add(second);
		
		return player_hand.hand.peek();
	}
	
	public Card removeTopOfHand()
	{
		Card top_of_hand = player_hand.hand.remove();
		
		Card first = player_hand.hand.remove();
		Card second = player_hand.hand.remove();
		
		player_hand.hand.add(first);
		player_hand.hand.add(second);
				
		
		return top_of_hand;
	}

	public void swapHand()
	{
		Card first = player_hand.hand.remove();
		Card second = player_hand.hand.remove();
		
		player_hand.hand.add(first);
		player_hand.hand.add(second);
		
	}
	
	public boolean checkIfWin()
	{
		if (player_deck == null) {return true; }
		else { return false;}
	}
		
	public void getCards() {

		for (int i = 0; i < 40; i++)
		{
			cards.add(new Card());
		}
		
		for (int i = 0; i < 10; i++ )
		{
		    cards.get(i).setColor(Color.BLUE);
			cards.get(i).setValue(i+1);
		}
		
		for (int i = 10; i < 20; i++ )
		{
			cards.get(i).setColor(Color.YELLOW);
			cards.get(i).setValue(i%10+1);
		}
		
		for (int i = 20; i < 30; i++ )
		{
			cards.get(i).setColor(Color.RED);
			cards.get(i).setValue(i%10+1);
		}
		
		for (int i = 30; i < 40; i++ )
		{
			cards.get(i).setColor(Color.GREEN);
			cards.get(i).setValue(i%10+1);
		}		
	}	
	public void shuffleCards() {
		
		for (int i = 1; i < 40; i++)
		{
			long seed = System.nanoTime();
			Collections.shuffle(cards, new Random(seed));
			
		}
	}
	
	public void dealCards() {
		
		for (int i = 0; i < 3; i++)
		{
			player_slot.slot[i] = this.cards.get(i);
		}
		
		for (int i = 0; i < 10; i++)
		{
			player_deck.deck.add(this.cards.get(i+3));
		}
		
		for (int i = 0; i < 27; i++)
		{
			player_hand.hand.add(this.cards.get(i+13));
		}
	}
	
	public void play() {}
}
