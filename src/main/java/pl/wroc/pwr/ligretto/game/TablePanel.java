package pl.wroc.pwr.ligretto.game;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import pl.wroc.pwr.ligretto.model.Table;

public class TablePanel extends JPanel implements ChangeListener{

	//private Table table;
	
	public TablePanel(Table table) {
		createTableContent(table);
	}
	
	private void createTableContent(Table table) {
				
		this.setLayout(new GridLayout(4, 4));
		this.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		/*for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				JButton table_button = new JButton(String.valueOf(table.ceckTable(k).getValue()));
				table_button.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
				table_button.setBackground(table.ceckTable(k).getColor());
				this.add(table_button);
				
				ChangeListener l = new ChangeListener() {
					
					@Override
					public void stateChanged(ChangeEvent e) {
						table_button.setText(String.valueOf(table.ceckTable(k).getValue()));
						table_button.setBackground(table.ceckTable(k).getColor());
						
					}
				};
				
				table_button.addChangeListener(l);
			}
		}
		*/
		JButton table_button_0 = new JButton(String.valueOf(table.ceckTable(0).getValue()));
		table_button_0.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_0.setBackground(table.ceckTable(0).getColor());
		this.add(table_button_0);
		
		ChangeListener l_0 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_0.setText(String.valueOf(table.ceckTable(0).getValue()));
				table_button_0.setBackground(table.ceckTable(0).getColor());
				
			}
		};
		
		table_button_0.addChangeListener(l_0);
		
		JButton table_button_1 = new JButton(String.valueOf(table.ceckTable(1).getValue()));
		table_button_1.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_1.setBackground(table.ceckTable(1).getColor());
		this.add(table_button_1);
		
		ChangeListener l_1 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_1.setText(String.valueOf(table.ceckTable(1).getValue()));
				table_button_1.setBackground(table.ceckTable(1).getColor());
				
			}
		};
		
		table_button_1.addChangeListener(l_1);
		
		JButton table_button_2 = new JButton(String.valueOf(table.ceckTable(2).getValue()));
		table_button_2.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_2.setBackground(table.ceckTable(2).getColor());
		this.add(table_button_2);
		
		ChangeListener l_2 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_2.setText(String.valueOf(table.ceckTable(2).getValue()));
				table_button_2.setBackground(table.ceckTable(2).getColor());
				
			}
		};
		
		table_button_2.addChangeListener(l_2);
		
		JButton table_button_3 = new JButton(String.valueOf(table.ceckTable(3).getValue()));
		table_button_3.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_3.setBackground(table.ceckTable(3).getColor());
		this.add(table_button_3);
		
		ChangeListener l_3 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_3.setText(String.valueOf(table.ceckTable(3).getValue()));
				table_button_3.setBackground(table.ceckTable(3).getColor());
				
			}
		};
		
		table_button_3.addChangeListener(l_3);
		
		JButton table_button_4 = new JButton(String.valueOf(table.ceckTable(4).getValue()));
		table_button_4.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_4.setBackground(table.ceckTable(4).getColor());
		this.add(table_button_4);
		
		ChangeListener l_4 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_4.setText(String.valueOf(table.ceckTable(4).getValue()));
				table_button_4.setBackground(table.ceckTable(4).getColor());
				
			}
		};
		
		table_button_4.addChangeListener(l_4);
		
		JButton table_button_5 = new JButton(String.valueOf(table.ceckTable(5).getValue()));
		table_button_5.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_5.setBackground(table.ceckTable(5).getColor());
		this.add(table_button_5);
		
		ChangeListener l_5 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_5.setText(String.valueOf(table.ceckTable(5).getValue()));
				table_button_5.setBackground(table.ceckTable(5).getColor());
				
			}
		};
		
		table_button_5.addChangeListener(l_5);
		
		JButton table_button_6 = new JButton(String.valueOf(table.ceckTable(6).getValue()));
		table_button_6.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_6.setBackground(table.ceckTable(6).getColor());
		this.add(table_button_6);
		
		ChangeListener l_6 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_6.setText(String.valueOf(table.ceckTable(6).getValue()));
				table_button_6.setBackground(table.ceckTable(6).getColor());
				
			}
		};
		
		table_button_6.addChangeListener(l_6);
		
		JButton table_button_7 = new JButton(String.valueOf(table.ceckTable(7).getValue()));
		table_button_7.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_7.setBackground(table.ceckTable(7).getColor());
		this.add(table_button_7);
		
		ChangeListener l_7 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_7.setText(String.valueOf(table.ceckTable(7).getValue()));
				table_button_7.setBackground(table.ceckTable(7).getColor());
				
			}
		};
		
		table_button_7.addChangeListener(l_7);
		
		JButton table_button_8 = new JButton(String.valueOf(table.ceckTable(8).getValue()));
		table_button_8.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_8.setBackground(table.ceckTable(8).getColor());
		this.add(table_button_8);
		
		ChangeListener l_8 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_8.setText(String.valueOf(table.ceckTable(8).getValue()));
				table_button_8.setBackground(table.ceckTable(8).getColor());
				
			}
		};
		
		table_button_8.addChangeListener(l_8);
		
		JButton table_button_9 = new JButton(String.valueOf(table.ceckTable(9).getValue()));
		table_button_9.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_9.setBackground(table.ceckTable(9).getColor());
		this.add(table_button_9);
		
		ChangeListener l_9 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_9.setText(String.valueOf(table.ceckTable(9).getValue()));
				table_button_9.setBackground(table.ceckTable(9).getColor());
				
			}
		};
		
		table_button_9.addChangeListener(l_9);
		
		JButton table_button_10 = new JButton(String.valueOf(table.ceckTable(10).getValue()));
		table_button_10.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_10.setBackground(table.ceckTable(10).getColor());
		this.add(table_button_10);
		
		ChangeListener l_10 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_10.setText(String.valueOf(table.ceckTable(10).getValue()));
				table_button_10.setBackground(table.ceckTable(10).getColor());
				
			}
		};
		
		table_button_10.addChangeListener(l_10);
		
		JButton table_button_11 = new JButton(String.valueOf(table.ceckTable(11).getValue()));
		table_button_11.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_11.setBackground(table.ceckTable(11).getColor());
		this.add(table_button_11);
		
		ChangeListener l_11 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_11.setText(String.valueOf(table.ceckTable(11).getValue()));
				table_button_11.setBackground(table.ceckTable(11).getColor());
				
			}
		};
		
		table_button_11.addChangeListener(l_11);
		
		JButton table_button_12 = new JButton(String.valueOf(table.ceckTable(12).getValue()));
		table_button_12.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_12.setBackground(table.ceckTable(12).getColor());
		this.add(table_button_12);
		
		ChangeListener l_12 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_12.setText(String.valueOf(table.ceckTable(12).getValue()));
				table_button_12.setBackground(table.ceckTable(12).getColor());
				
			}
		};
		
		table_button_12.addChangeListener(l_12);
	
		JButton table_button_13 = new JButton(String.valueOf(table.ceckTable(13).getValue()));
		table_button_13.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_13.setBackground(table.ceckTable(13).getColor());
		this.add(table_button_13);
		
		ChangeListener l_13 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_13.setText(String.valueOf(table.ceckTable(13).getValue()));
				table_button_13.setBackground(table.ceckTable(13).getColor());
				
			}
		};
		
		table_button_13.addChangeListener(l_13);
		
		JButton table_button_14 = new JButton(String.valueOf(table.ceckTable(14).getValue()));
		table_button_14.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_14.setBackground(table.ceckTable(14).getColor());
		this.add(table_button_14);
		
		ChangeListener l_14 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_14.setText(String.valueOf(table.ceckTable(14).getValue()));
				table_button_14.setBackground(table.ceckTable(14).getColor());
				
			}
		};
		
		table_button_14.addChangeListener(l_14);
		
		JButton table_button_15 = new JButton(String.valueOf(table.ceckTable(15).getValue()));
		table_button_15.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
		table_button_15.setBackground(table.ceckTable(15).getColor());
		this.add(table_button_15);
		
		ChangeListener l_15 = new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				table_button_15.setText(String.valueOf(table.ceckTable(15).getValue()));
				table_button_15.setBackground(table.ceckTable(15).getColor());
				
			}
		};
		
		table_button_15.addChangeListener(l_15);
		
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	//public static JPanel createTablePanel() {
	//	return new TablePanel(Game);
	//}
	
}
