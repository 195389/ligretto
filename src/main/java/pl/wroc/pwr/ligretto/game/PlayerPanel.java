package pl.wroc.pwr.ligretto.game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import pl.wroc.pwr.ligretto.model.Player;
import pl.wroc.pwr.ligretto.model.Table;

public class PlayerPanel extends JPanel {

	private Player player;
	private Table table;

	public PlayerPanel(Color color, Player player, Table table) {
		createPlayerPanelContent(color, player, table);
	}

	private void createPlayerPanelContent(Color color, Player player,
			Table table) {

		JButton row_1_button = new JButton(String.valueOf(player.checkSlot(0)
				.getValue()));
		this.add(row_1_button);
		row_1_button.setBackground(player.checkSlot(0).getColor());
		if (player.checkSlot(0).getColor() == Color.GREEN
				|| player.checkSlot(0).getColor() == Color.YELLOW) {
			row_1_button.setForeground(Color.BLACK);
		}
		if (player.checkSlot(0).getColor() == Color.BLUE
				|| player.checkSlot(0).getColor() == Color.RED) {
			row_1_button.setForeground(Color.WHITE);
		}

		ActionListener row_1_listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (player.checkSlot(0) != null) {
					// System.out.println(player.removeSlot(0).getValue());
					for (int i = 0; i < 16; i++) {
						
						if(player.checkSlot(0).getValue() == table.ceckTable(i).getValue()+1 && (table.ceckTable(i).getColor() == Color.GRAY || table.ceckTable(i).getColor() == player.checkSlot(0).getColor())) {table.changeTable(player.removeSlot(0), i);break;}
						
					}
					if (player.checkSlot(0) != null) {
						row_1_button.setBackground(player.checkSlot(0)
								.getColor());
						row_1_button.setText(String.valueOf(player
								.checkSlot(0).getValue()));
					} else {
						row_1_button.setBackground(Color.GRAY);
						row_1_button.setText("0");
						//break;
					}
				}
			}
		};
		row_1_button.addActionListener(row_1_listener);

		JButton row_2_button = new JButton(String.valueOf(player.checkSlot(1)
				.getValue()));
		this.add(row_2_button);
		row_2_button.setBackground(player.checkSlot(1).getColor());
		if (player.checkSlot(1).getColor() == Color.GREEN
				|| player.checkSlot(1).getColor() == Color.YELLOW) {
			row_2_button.setForeground(Color.BLACK);
		}
		if (player.checkSlot(1).getColor() == Color.BLUE
				|| player.checkSlot(1).getColor() == Color.RED) {
			row_2_button.setForeground(Color.WHITE);
		}

		ActionListener row_2_listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (player.checkSlot(1) != null) {
					// System.out.println(player.removeSlot(0).getValue());
					for (int i = 0; i < 16; i++) {
						
						if(player.checkSlot(1).getValue() == table.ceckTable(i).getValue()+1 && (table.ceckTable(i).getColor() == Color.GRAY || table.ceckTable(i).getColor() == player.checkSlot(1).getColor())) {table.changeTable(player.removeSlot(1), i);break;}
						
					}
					if (player.checkSlot(1) != null) {
						row_2_button.setBackground(player.checkSlot(1)
								.getColor());
						row_2_button.setText(String.valueOf(player
								.checkSlot(1).getValue()));
						//break;
					} else {
						row_2_button.setBackground(Color.GRAY);
						row_2_button.setText("0");
						//break;
					}
				}
			}
		};
		row_2_button.addActionListener(row_2_listener);
		
		
		
		JButton row_3_button = new JButton(String.valueOf(player.checkSlot(2)
				.getValue()));
		this.add(row_3_button);
		row_3_button.setBackground(player.checkSlot(2).getColor());
		if (player.checkSlot(2).getColor() == Color.GREEN
				|| player.checkSlot(2).getColor() == Color.YELLOW) {
			row_3_button.setForeground(Color.BLACK);
		}
		if (player.checkSlot(2).getColor() == Color.BLUE
				|| player.checkSlot(2).getColor() == Color.RED) {
			row_3_button.setForeground(Color.WHITE);
		}

		ActionListener row_3_listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (player.checkSlot(2) != null) {
					// System.out.println(player.removeSlot(0).getValue());
					for (int i = 0; i < 16; i++) {
						
						if(player.checkSlot(2).getValue() == table.ceckTable(i).getValue()+1 && (table.ceckTable(i).getColor() == Color.GRAY || table.ceckTable(i).getColor() == player.checkSlot(2).getColor())) {table.changeTable(player.removeSlot(2), i);break;}
						
					}
					if (player.checkSlot(2) != null) {
						row_3_button.setBackground(player.checkSlot(2)
								.getColor());
						row_3_button.setText(String.valueOf(player
								.checkSlot(2).getValue()));
						//break;
					} else {
						row_3_button.setBackground(Color.GRAY);
						row_3_button.setText("0");
						//break;
					}
				}
			}
		};
		row_3_button.addActionListener(row_3_listener);
		
		JButton hand_button = new JButton(String.valueOf(player.checkHand()
				.getValue()));
		this.add(hand_button);
		hand_button.setBackground(player.checkHand().getColor());
		if (player.checkHand().getColor() == Color.GREEN
				|| player.checkHand().getColor() == Color.YELLOW) {
			hand_button.setForeground(Color.BLACK);
		}
		if (player.checkHand().getColor() == Color.BLUE
				|| player.checkHand().getColor() == Color.RED) {
			hand_button.setForeground(Color.WHITE);
		}

		ActionListener hand_listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (player.checkSlot(2) != null) {
					// System.out.println(player.removeSlot(0).getValue());
					for (int i = 0; i < 16; i++) {
						
						if(player.checkHand().getValue() == table.ceckTable(i).getValue()+1 && (table.ceckTable(i).getColor() == Color.GRAY || table.ceckTable(i).getColor() == player.checkHand().getColor())) {table.changeTable(player.removeTopOfHand(), i);break;}
						//else {player.swapHand();}
						
					}
					player.swapHand();
					if (player.checkHand() != null) {
						hand_button.setBackground(player.checkHand()
								.getColor());
						hand_button.setText(String.valueOf(player
								.checkHand().getValue()));
						//break;
					} else {
						hand_button.setBackground(Color.GRAY);
						hand_button.setText("0");
						//break;
					}
				}
			}
		};
		hand_button.addActionListener(hand_listener);
	
		
		JButton deck_button = new JButton(String.valueOf(player
				.checkTopOfDeck().getValue()));
		this.add(deck_button);
		deck_button.setBackground(player.checkTopOfDeck().getColor());
		if (player.checkTopOfDeck().getColor() == Color.GREEN
				|| player.checkTopOfDeck().getColor() == Color.YELLOW) {
			deck_button.setForeground(Color.BLACK);
		}
		if (player.checkTopOfDeck().getColor() == Color.BLUE
				|| player.checkTopOfDeck().getColor() == Color.RED) {
			deck_button.setForeground(Color.WHITE);
		}

		
		ActionListener deck_listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (player.checkSlot(2) != null) {
					// System.out.println(player.removeSlot(0).getValue());
					for (int i = 0; i < 16; i++) {
						
						if(player.checkTopOfDeck().getValue() == table.ceckTable(i).getValue()+1 && (table.ceckTable(i).getColor() == Color.GRAY || table.ceckTable(i).getColor() == player.checkTopOfDeck().getColor())) {table.changeTable(player.removeTopOfDeck(), i);break;}
						
					}
					if (player.checkTopOfDeck() != null) {
						deck_button.setBackground(player.checkTopOfDeck()
								.getColor());
						deck_button.setText(String.valueOf(player
								.checkTopOfDeck().getValue()));
						//break;
					} else {
						deck_button.setBackground(Color.GRAY);
						deck_button.setText("0");
						//break;
					}
				}
			}
		};
		deck_button.addActionListener(deck_listener);
		
		
		this.add(Box.createRigidArea(new Dimension(10, 10)));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		// this.setLayout(new GridLayout(1, 5, 1, 1));

	}

	public static JPanel createPlayerPanel(Color color, Player player,
			Table table) {
		return new PlayerPanel(color, player, table);
	}

}
