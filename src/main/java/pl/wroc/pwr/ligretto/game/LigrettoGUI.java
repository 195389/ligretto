package pl.wroc.pwr.ligretto.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import pl.wroc.pwr.ligretto.model.Table;

public class LigrettoGUI {
	
	public void createGUI(Game game) {
		JFrame frame = new JFrame();
		frame.setTitle("Ligretto");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(500, 500));
		
		TablePanel table = new TablePanel(game.getTable());
		frame.getContentPane().add(BorderLayout.CENTER, table);
		
		PlayerPanel panel_1 = new PlayerPanel(Color.BLUE,game.getPlayers(0),game.getTable());
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		frame.getContentPane().add(BorderLayout.SOUTH, panel_1);
		
		PlayerPanel panel_2 = new PlayerPanel(Color.GREEN,game.getPlayers(1),game.getTable());
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		frame.getContentPane().add(BorderLayout.NORTH, panel_2);

		PlayerPanel panel_3 = new PlayerPanel(Color.YELLOW, game.getPlayers(2),game.getTable());
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));
		frame.getContentPane().add(BorderLayout.WEST, panel_3);
		

		PlayerPanel panel_4 = new PlayerPanel(Color.RED, game.getPlayers(3),game.getTable());
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		frame.getContentPane().add(BorderLayout.EAST, panel_4);
		
		
		
		
		frame.setVisible(true);		
		
	}
	

}
