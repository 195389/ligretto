package pl.wroc.pwr.ligretto.game;

import java.util.ArrayList;
import java.util.List;

import pl.wroc.pwr.ligretto.model.CardSet;
import pl.wroc.pwr.ligretto.model.Player;
import pl.wroc.pwr.ligretto.model.Table;

public class Game {

	private CardSet cardSet;
	private List<Player> players;
	private Table game_table;
	
	public Game() {
		cardSet = new CardSet();
		players = new ArrayList<Player>();
		game_table = new Table();
		
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		
		Player player1 = new Player("Gracz");
		Player player2 = new Player("Komputer1");
		Player player3 = new Player("Komputer2");
		Player player4 = new Player("Komputer3");
		
		game.addPlayer(player1);
		game.addPlayer(player2);
		game.addPlayer(player3);
		game.addPlayer(player4);

		player1.getCards();
		player1.shuffleCards();
		player1.dealCards();
		
		player2.getCards();
		player2.shuffleCards();
		player2.dealCards();
		
		player3.getCards();
		player3.shuffleCards();
		player3.dealCards();
		
		player4.getCards();
		player4.shuffleCards();
		player4.dealCards();
		
		LigrettoGUI gui = new LigrettoGUI();
		gui.createGUI(game);
		
		//System.out.println("Rząd: ");
		//player1.checkSlot(0).showCard();
		//player1.checkSlot(1).showCard();
		//player1.checkSlot(2).showCard();
		//System.out.println("Sterta: ");
		//player1.checkTopOfDeck().showCard();
		//System.out.println("Ręka: ");
		//player1.checkHand().showCard();
		
		
		
		while (game.play(player1, player2, player3, player4) != false)
			{
			game.play(player1, player2, player3, player4);
			}

	}
	
	public CardSet getCardSet() {
		return cardSet;
	}

	public Player getPlayers(int i) {
		return players.get(i);
	}

	public Table getTable() {
		return game_table;
	}
	
	public boolean play(Player player1, Player player2, Player player3, Player player4) {
		
		if(player1.checkIfWin() == true || player2.checkIfWin() == true || player3.checkIfWin() == true || player4.checkIfWin() == true) {return true;}
		return false;
		
	}

	public void addPlayer(Player player) {
		this.players.add(player);
	
	}
	
}
